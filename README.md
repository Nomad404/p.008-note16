#Note 16
##Teilnehmer
Philipp Kostanski, Max Kruse, Eva-Maria Meier

##Projektbeschreibung
Es wird eine interaktive Webseite entwickelt welche durch Interaktion seitens dem Nutzer eine Melodie bzw. Klangfolge mit Tönen und Samples generiert. Diese Klangfolge wird im weiteren abspielbar sein. Hierbei können durch den Nutzer auch Tonfolgen und / oder Samples aneinandergereiht werden, welche eine Melodie ergeben. Die Interaktion des Nutzers mit der Webseite wird mithilfe von Mausklicks stattfinden. Hierbei wird auf dem UI eine Matrix (Änderungen vorbehalten) dargestellt, bei der der Nutzer auf die Items der Matriks klicken kann und dadurch ein Ton ausgewählt wurde. Durch das erneute Klicken auf ein anderes Feld wird eine Tonfolge generiert.
Beispiele: http://superlooper.universlabs.co.uk/ oder http://tonematrix.audiotool.com/

##Anforderungen
###Kernfeatures 
* SinglePage-Anwendung zum interaktiven Erstellen von Klangfolgen  
* Töne können zu Klangfolgen zusammengestellt werden  
* Mehrere Klangfolgen können angelegt werden 
* Nutzer kann Töne auswählen/modifizieren  
* Abspielen der erstellten Inhalte 
* Katalog mit verschiedenen Samples (Drums, Beats, ...)  
* Globales Einstellen des Tempos (BMP) 
* Anwendungszustand speichern 

###Zusatzfeatures 
* Abspeichern der Komposition als MIDI/MP3   
* Einstellen der Matrixgröße   
* Effekte auf Klangfolge (Samples) 
* Einstellen der Tonart und der Oktavenhöhe   
* Tracks erstellen in Chiptune / 8­Bit­Style   
* Anordnung der Tracks/Noten in einer Art Rubixwürfel 

###Anleitung zum Starten
Die Anwendung lässt sich über die "index.html" Datei starten. Alle Nutzer können die Seite unter http://132.199.139.24/~krm22840/Note16/index.html erreichen. Als Testen muss man alle Dateien auf einen Webserver hochladen, da XHRs benutzt werden.
