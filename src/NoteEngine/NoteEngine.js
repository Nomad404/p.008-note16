var NoteEngine = (function(){

	var that = {},
		matrices = [],				// Enthält alle Matrizenobjekte
		beatCount = 1,				// Anzahl der Takte
		globalBPM = 100,			// globales Tempo
		rhythmID = "rhythm0",		// ID des Rhythmus

	// Initialisiert die Engine
	// Erstellt zuerst den Audiokontext
	// Gleicht die Enginedaten mit den übergebenen Projektdaten ab
	// Stellt den BPM-Slider und die Rhythmus Auswahl im UI je nach Datenn ein
	// Erstellt alle Matrizenobjekte
	// Initialisiert den Rhythmushandler
	// Registriert alle Listener
	init = function(project){
		window.AudioContext = window.AudioContext || window.webkitAudioContext;
		context = new AudioContext();

		beatCount = project.beatCount;
		globalBPM = project.bpm;
		rhythmID = project.rhythmID;

		//$("#bpmSlider").val(globalBPM);
		//$("#bpmSlider").slider("refresh");
		$(".rhythmSel").removeClass("active");
		$("#" + rhythmID).addClass("active");

		var stats = project.stats;
		for(var i = 0; i < 4; i++){
			matrices[i] = new NoteMatrix(stats[i].instrument, (16 * beatCount), globalBPM, stats[i].stats);
		}

		RhythmHandler.init(globalBPM, parseInt(rhythmID.replace("rhythm", "")), beatCount);
		_registerListener();
	},

	// Registriert alle Listener
	_registerListener = function(){
		$("body").on("EXPORT_START", _initExport);
		$("body").on("BPM_CHANGE", _changeGlobalBPM);
		$("body").on("RHYTHM_CHANGE", _changeRhythm);
		$("body").on("REFRESH_ALL", function(){
			globalBPM = 100;
			rhythmID = "rhythm0";
		});
	},

	// Initialisiert den Export
	_initExport = function(){
		var result = {};
		result.bpm = globalBPM;
		result.beatCount = beatCount;
		result.rhythmID = rhythmID;
		
		var stats = [];
		for(var i = 0; i < matrices.length; i++){
			stats[i] = {};
			stats[i].instrument = matrices[i].getInstrument();
			stats[i].stats = matrices[i].getNoteStats();
		}
		result.stats = stats;
		document.getElementById("importExportTextfield").value = JSON.stringify(result);
		ExportDialog.init();
	},

	// Ändert die globale BPM (für Export)
	_changeGlobalBPM = function(event, data){
		globalBPM = parseInt(data);
	},

	// Ändert die Rhythmus Einstellung (für Export)
	_changeRhythm = function(event, data){
		rhythmID = data;
	};

	that.init = init;
	return that;
})();