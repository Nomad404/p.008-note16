function NoteRow(instrument, loopLength, bpm, row, minID, maxID, buffer, stats){
	
	var noteBuffer = buffer,			// Notenklang in Bufferform
		gainNode = null,				// Hauptknoten für Ausgabe (auch lautstärke)
		volume,							// Gesamtlautstärke
		notes = [],						// Enthält alle Notenzustände in der Schleife (taktübergreifend)
		rowInstrument = instrument,		// Instrumentname in Stringform (für Abgleiche)
		timeouts = [],					// Zuständig für Wiederholung/Stop der Schleife
		isPlaying = false,				// Status ob Schleife spielt oder nicht
		current16thNote = 0,			// Aktuelle Position in der Schleife
		rowBPM = bpm,					// Tempo der Schleife
		scheduleDelay = 0,				// Verzögerung der Wiederholung der Planschleife (in ms) 
		scheduleAheadTime = 0.1,		// Abdeckung der Planschleife (in s)
		nextNoteTime = 0.0;				// Startzeit der nächsten Note

	_init();
	_registerListener();

	// Initialisiert die Notenreihe
	function _init(){
		gainNode = context.createGain();
		volume = 2.5;
		gainNode.gain.value = volume;
		for(var i = 0; i < loopLength; i++){
			notes[i] = stats[i];
			if(notes[i]){
				$("#" + (minID + i) + rowInstrument).removeClass("unclicked");
				$("#" + (minID + i) + rowInstrument).addClass("clicked");
			}
		}
	}

	// Registriert alle Listener für die Notenreihe
	function _registerListener(){
		$("body").on("CELL_CLICKED", _toggleNote);
		$("body").on("PLAY", _handlePlayback);
		$("body").on("STOP", _handlePlayback);
		$("body").on("VOLUME_CHANGE", _changeVolume);
		$("body").on("BPM_CHANGE", _changeBPM);
		$("body").on("MUTE", _mute);
		$("body").on("RESUME_SOUND", _resumeSound);
		$("body").on("REFRESH_ALL", _refresh);
	}

	// Prüft, ob die gesendete Note in der Reihe mit dem eingestellten Instrument enthalten ist
	// und schaltet sie ggf. um
	function _toggleNote(event, data){
		var dataString = data.id;
		if(dataString.indexOf(rowInstrument) > -1){
			var number = parseInt(dataString.replace(instrument, ""));
			if(number >= minID && number <= maxID){
				//notes[number - minID] = !notes[number - minID];
				if(notes[number - minID] == 0){
					notes[number - minID] = 1;
				}else{
					notes[number - minID] = 0;
				}
			}
		}
	}

	// Reagiert auf Play/Stop Event
	function _handlePlayback(){
		isPlaying = !isPlaying;

		if(isPlaying) { 
			current16thNote = 0;
			nextNoteTime = context.currentTime;
			_startScheduler();
		}else{
			for(var i = 0; i < timeouts.length; i++){
				clearTimeout(timeouts[i]);
			}
			timeouts = [];
		}
	}

	// Schaltet die Notenreihe stumm
	function _mute(){
		gainNode.gain.value = 0;
	}

	// Stellt die ursprüngliche Lautstärke der Notenreihe wieder her
	function _resumeSound(){
		gainNode.gain.value = volume;
	}

	// Setzt die Notenreihe zurück
	function _refresh(){
		for(var i = 0; i < notes.length; i++){
			notes[i] = false;
		}
	}

	// Ändert die Lautstärke der Notenreihe
	function _changeVolume(event, data){
		volume = data/20;
		gainNode.gain.value = volume;
	}

	// Ändert das Tempo der Notenreihe
	function _changeBPM(event, data){
		rowBPM = data;
	}

	// Startet die Playback Schleife, die immer wieder abprüft, 
	// ob im vorgelegten Zeitraum eine Note abgespielt werden soll
	function _startScheduler(){
		while (nextNoteTime < context.currentTime + scheduleAheadTime ) {
			_scheduleNote(current16thNote, nextNoteTime);
			_nextNote();
		}
		timeouts.push(setTimeout(_startScheduler, scheduleDelay));
	}

	// Spielt die Note und aktiviert die entsprechende Animation
	// Zusätzlich wird die Zeitleiste je nach Takt verändert
	function _scheduleNote(beatPosition, time){
		if(notes[beatPosition] == 1){
			var voice = context.createBufferSource();
			voice.buffer = noteBuffer;
			voice.connect(gainNode);
			gainNode.connect(context.destination);
			voice.start(time);

			$("#" + (minID + beatPosition) + rowInstrument).addClass("animation");
			setTimeout(function(){
				$("#" + (minID + beatPosition) + rowInstrument).removeClass("animation");
			}, 100);
		}

		/*if(beatPosition % 16 == 0){
			var activeNumber = (beatPosition/16) | 0;
			for(var i = 0; i < ((loopLength/16) | 0); i++){
				$("#beatTab" + i).removeClass("active");
				$("#beat" + i).removeClass("active");
			}
			$("#beatTab" + activeNumber).addClass("active");
			$("#beat" + activeNumber).addClass("active");
		}*/

		if(beatPosition % 16 == 0){
			var activeNumber = (beatPosition/16) | 0;
			for(var i = 0; i < ((loopLength/16) | 0); i++){
				$("#timeLine" + i).removeClass("current");
			}
			$("#timeLine" + activeNumber).addClass("current");
		}
	}

	// Verschiebt die Position der Schleife nach vorne
	// Abhängig vom Tempo legt es auch das Interval zur nächsten Note fest
	function _nextNote(){
		var secondsPerBeat = 60.0 / rowBPM;
		nextNoteTime += 0.25 * secondsPerBeat;

		current16thNote++; 
		if (current16thNote == loopLength) {
			current16thNote = 0;
		}
	}

	// Gibt die aktuelle Einstellung der Notenreihe zurück
	this.getNoteStats = function(){
		return notes;
	}
}