function NoteMatrix(instrument, colCount, bpm, stats){

	var rows = [],							// Alle Reihen der Matrix
		matrixInstrument = instrument,		// Eingestelltes Instrument
		bufferLoader,						// Bufferloader Objekt
		matrixStats = stats,				// Matrix mit allen Daten der Noten (ob an oder aus)
		bufferList;							// Liste die die Buffer der Noten enthält

	_loadBuffer();

	// Lädt den Bufferloader mit allen Sounds des übergebenen Instruments
	function _loadBuffer(){
		var notePaths = _createNoteFilePaths();

		bufferLoader = new BufferLoader(
			context,
			notePaths,
			_finishedLoading);
		bufferLoader.load();
	}

	// Erstellt ein Array mit allen Soundpfaden eines Instruments
	function _createNoteFilePaths(){
		var basePath = "res/sounds/" + matrixInstrument + "/",
			scale = ['C6', 'A5', 'G5', 'E5', 'D5', 'C5', 'A4', 'G4', 'E4', 'D4', 'C4', 'A3', 'G3', 'E3', 'D3', 'C3'],
			result = [];
		for(var i = 0; i < 16; i++){
			result[i] = basePath + scale[i] + ".mp3";
		}
		return result;
	}

	// Wird beim abschließen des Bufferloaders ausgelöst und verabeitet die resultierenden Buffer
	function _finishedLoading(buffer){
		$("body").trigger("MODULE_FINISHED");
		bufferList = buffer;
		_createMatrix();
	}

	// Erstellt alle Notenreihen
	function _createMatrix(){
		for(var i = 0; i < 16; i++){
			rows[i] = new NoteRow(matrixInstrument, colCount, bpm, i, (i*colCount), (((i+1)*colCount) - 1), bufferList[i], matrixStats[i]);
		}
	}

	// Gibt den Status der gesamten Matrix zurück
	this.getNoteStats = function(){
		var result = [];
		for(var i = 0; i < rows.length; i++){
			result[i] = rows[i].getNoteStats();
		}
		return result;
	}

	// Gibt den namen des Instruments der Matrix zurück
	this.getInstrument = function(){
		return matrixInstrument;
	}
}