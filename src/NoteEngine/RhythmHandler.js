var RhythmHandler = (function(){

	var that = {},						
		bufferLoader,						// Bufferloader Objekt
		bufferList,							// Liste aller Sounds in Bufferform
		rhythmID = 0,						// ID des aktuellen Rhythmus
		rhythmBPM,							// Tempo des Rhythmus in BPM
		currentRhythm,						// Aktueller Rhythmus
		gainNode = null,					// Audioausgabeknoten
		volume,								// Lautstärke
		timeouts = [],						// Timeoutliste
		currentBeatPosition = 0,			// Aktuelle Position im Takt
		loopLength = 16,					// Länge der Schleife in 16tel Schritten
		isPlaying = 0,						// Spielt die Schleife gerade?
		scheduleDelay = 0,					// Verzögerung der Planschleife
		scheduleAheadTime = 0.1,			// Scanintervall der Planschleife
		nextNoteTime = 0.0,					// Zeit, wann die nächste Note abgespielt werden soll

		// Vorlage der Rhythmen
		rhythmTemplates = [
			[
				[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
				[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
				[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
				[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
				[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
				[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
				[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
				[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
				[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
				[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
				[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
			],
			[
				[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
				[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
				[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
				[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
				[0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0],
				[1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0],
				[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
				[0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0],
				[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
				[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
				[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
				[1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0]
			],
			[
				[0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0],
				[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
				[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
				[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
				[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
				[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
				[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
				[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
				[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
				[1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0],
				[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
				[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
			],
			[
				[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0],
				[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
				[1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0],
				[0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0],
				[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
				[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
				[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
				[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
				[0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0],
				[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
				[1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0],
				[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
			]
		];

	// Initialisiert den Handler
	init = function(bpm, index, beatCount){
		rhythmBPM = bpm;
		rhythmID = index;
		currentRhythm = rhythmTemplates[index];

		gainNode = context.createGain();
		volume = 0.5;
		gainNode.gain.value = volume;

		_registerListener();
		_loadBuffer();
	},

	// Registriert alle Listener
	_registerListener = function(){
		$("body").on("PLAY", _handlePlayback);
		$("body").on("STOP", _handlePlayback);
		$("body").on("VOLUME_CHANGE", _changeVolume);
		$("body").on("BPM_CHANGE", _changeBPM);
		$("body").on("MUTE", _mute);
		$("body").on("RESUME_SOUND", _resumeSound);
		$("body").on("RHYTHM_CHANGE", function(event, data){
			var number = parseInt(data.replace("rhythm", ""));
			rhythmID = number;
			currentRhythm = rhythmTemplates[rhythmID];
		});
		$("body").on("REFRESH_ALL", function(){
			rhythmID = 0;
			currentRhythm = rhythmTemplates[0];
		});
	},

	// Lädt alle Sounds in den Bufferloader
	_loadBuffer = function(){
		var drumPaths = _createDrumPaths();

		bufferLoader = new BufferLoader(
			context,
			drumPaths,
			_finishedLoading);
		bufferLoader.load();
	},

	// Generiert eine Liste der Pfade von allen Sounds
	_createDrumPaths = function(){
		var result = [],
			basePath = "res/sounds/kit/";
			sounds = [
			"Clap_01.wav", 
			"Shaker_02.wav", 
			"HiHat_01.wav", 
			"HiHat_01_muted.wav", 
			"HiHat_02.wav", 
			"HiHat_03.wav", 
			"HiHatBig_01.wav", 
			"Snare_01.wav",
			"Snare_03.wav",
			"Tom_01.wav",
			"Kick_02.wav",
			"Kick_03.wav"];
		for(var i = 0; i < sounds.length; i++){
			result[i] = basePath + sounds[i];
		}
		return result;
	},

	// Wird ausgelöst, sobald der Bufferloader fertiggeladen wurde
	// Passt den Handler an
	_finishedLoading = function(bList){
		bufferList = bList;
		currentRhythm = rhythmTemplates[rhythmID];
		$("body").trigger("MODULE_FINISHED");
	},

	// Reagiert entsprechend auf Play/Stop Event
	_handlePlayback = function(){
		isPlaying = !isPlaying;

		if(isPlaying){
			currentBeatPosition = 0;
			nextNoteTime = context.currentTime;
			_startScheduler();
		}else{
			for(var i = 0; i < timeouts.length; i++){
				clearTimeout(timeouts[i]);
			}
			timeouts = [];
		}
	},

	// Startet die Playback Schleife, die immer wieder abprüft, 
	// ob im vorgelegten Zeitraum eine Note abgespielt werden soll
	_startScheduler = function(){
		while (nextNoteTime < context.currentTime + scheduleAheadTime ) {
			_scheduleNote(currentBeatPosition, nextNoteTime);
			_nextNote();
		}
		timeouts.push(setTimeout(_startScheduler, scheduleDelay));
	},

	// Spielt alle aktivierten Noten an der übergebenen Position
	_scheduleNote = function(beatPosition, time){
		for(var i = 0; i < currentRhythm.length; i++){
			if(currentRhythm[i][beatPosition] == 1){
				_playNote(bufferList[i], time);

				/*$("#" + (minID + beatPosition) + rowInstrument).addClass("animation");
				setTimeout(function(){
					$("#" + (minID + beatPosition) + rowInstrument).removeClass("animation");
				}, 100);*/
			}
		}
	},

	// Spielt den Buffer zur übergebenen Zeit ab
	_playNote = function(buffer, time){
		var sound = context.createBufferSource();
		sound.buffer = buffer;
		sound.connect(gainNode);
		gainNode.connect(context.destination);
		sound.start(time);
	},

	// Verschiebt die Position der Schleife nach vorne
	// Abhängig vom Tempo legt es auch das Interval zur nächsten Note fest
	_nextNote = function(){
		var secondsPerBeat = 60.0 / rhythmBPM;
		nextNoteTime += 0.25 * secondsPerBeat;

		currentBeatPosition++; 
		if (currentBeatPosition == loopLength) {
			currentBeatPosition = 0;
		}
	},

	// Schaltet den Rhythmus stumm
	_mute = function(){
		gainNode.gain.value = 0;
	},

	// Stellt die ursprüngliche Lautstärke wieder her
	_resumeSound = function(){
		gainNode.gain.value = volume;
	},

	// Ändert die Lautstärke 
	_changeVolume = function(event, data){
		volume = data/150;
		gainNode.gain.value = volume;
	},

	// Ändert das Tempo 
	_changeBPM = function(event, data){
		rhythmBPM = data;
	};

	that.init = init;
	return that;
})();