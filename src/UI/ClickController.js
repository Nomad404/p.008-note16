var ClickController = (function(){
	
	var that = {},
	clicked = false,				// Ist eine Maustaste gedrückt?
	drag = false, 					// Wird die Maus gezogen?
	cellActivate = false;			// Werden die angeklickten Zellen gerade aktiviert oder deaktiviert?

	// Initialisiert den ClickController
	init = function() {
		_registerListeners();
	},

	// Registriert alle Listener
	_registerListeners = function(){
		$(".matrixContainerSingle").on("mousedown", _onMouseDown);
		$(".matrixContainerSingle").on("mousemove", _onMouseMove);
		$(".matrixContainerSingle").on("mouseup", _onMouseUp);
		$(".matrixContainerSingle").on("mouseleave", _onMouseLeft);
	},

	// Reagiert auf Mousedown Event
	// Überprüft das angeklickte Element
	// Falls es ein <span> Element ist, stellt den Modus je nach 
	// angeklickter Zelle ein
	_onMouseDown = function(event){
		if(event.target.nodeName === "SPAN"){
			var cell = event.target;
			clicked = true;
			if(cell.className == "cell unclicked"){
				cellActivate = true;
			}else{
				cellActivate = false;
			}
		}
	},

	// Wird beim Mousemove Event ausgelöst
	// Überprüft welchen Status die Zellen haben und sendet sie
	// Schaltet sie außerdem im UI um und aktiviert die Animationen
	_onMouseMove = function(event){
		if(clicked){
			drag = true;
		}
		if(clicked && drag){
			if(event.target.nodeName === "SPAN"){
				var cell = event.target,
					cellData = {
						id: cell.id
					};
				if(cellActivate && cell.className !== "cell clicked" && cell.className !== "cell clicked animation"){
					cell.className = "cell clicked animation";
					setTimeout(function(){
						cell.className = "cell clicked";
					}, 100);
					$("body").trigger("CELL_CLICKED", cellData);
				}else if(cellActivate == false && cell.className !== "cell unclicked" && cell.className !== "cell unclicked animation"){
					cell.className = "cell unclicked animation";
					setTimeout(function(){
						cell.className = "cell unclicked";
					}, 100);
					$("body").trigger("CELL_CLICKED", cellData);
				}
			}
		}
	},

	// Wird beim MouseUp event ausgelöst
	// Verläuft genauso wie bei MouseMove
	_onMouseUp = function(event){
		if(event.target.nodeName === "SPAN"){
			var cell = event.target,
				cellData = {
					id: cell.id
				};
			if(clicked && drag == false){
				if(cellActivate && cell.className != "cell clicked"){
					cell.className = "cell clicked animation";
					setTimeout(function(){
						cell.className = "cell clicked";
					}, 100);
					$("body").trigger("CELL_CLICKED", cellData);
				}else if(cellActivate == false && cell.className != "cell unclicked"){
					cell.className = "cell clicked animation";
					setTimeout(function(){
						cell.className = "cell unclicked";
					}, 100);
					$("body").trigger("CELL_CLICKED", cellData);
				}
			}
		}
		clicked = false;
		drag = false;
	},

	// Schaltet bei Mouseleave den Status der Events aus
	_onMouseLeft = function(event){
		clicked = false;
		drag = false;
	};

	that.init = init;
	return that;
})();