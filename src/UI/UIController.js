var UIController = (function(){

	var that = {},
		gloabalBeatCount,

	//Initialisiert alle notwenigen UI-Module
	init = function(count){
		gloabalBeatCount = count;
		_registerListener();
		ControlsView.init();
		TabView.init(gloabalBeatCount);
	},

	// Registriert alle Listener
	_registerListener = function(){
		$("body").on("CONTROLSVIEW_FINISHED", function(){
			$("body").trigger("MODULE_FINISHED");
		})
				.on("TABVIEW_FINISHED", function(){
			MatrixView.init(gloabalBeatCount);
			$("body").trigger("MODULE_FINISHED");
		})
				.on("MATRIXVIEW_FINISHED", function(){
			ClickController.init();
			$("body").trigger("MODULE_FINISHED");
		});
	};

	that.init = init;
	return that;

})();