var IntroView = (function(){

	var that = {},
		barSelection = 1,
	
	// Initialisiert das IntroView
	init = function() {
		_resposDialog(); 
		$('#introDialog').modal({
			show:true,
			backdrop:"static",
			keyboard:true
		}); 
		_registerListeners();
	},

	// Registriert alle Listener
	_registerListeners = function(){
		$('#btnDialogCreate').on("click", _loadPreloader); 
		$(".beatCountSelection").on("click", function(event){
			barSelection = parseInt(event.target.innerText);
		});
		$("body").on("PRELOAD_FINISHED", function(){
			$("#loadingDialog").toggle();
		});
	},

	// Positioniert das IntroView an die richtige Stelle
	_resposDialog = function(){
		window.onload = function(){
			$dialog = $(".modal-dialog");
			var dialogHeight = parseInt($(".modal-dialog").css("height").replace("px", "")),
				screenHeight = window.screen.height;
			$(".modal-dialog").css("margin-top", ((screenHeight/2) - (dialogHeight/2) - (screenHeight*0.07)) + "px");
		}
	},

	// Startet den Ladevorgang der Anwendung
	_loadPreloader = function(){
		var importText = document.getElementById("introImportTextfield").value;
		$('#introDialog').modal({show:false});
		$(document).ready(function(){
        	$('#loadingDialog').modal({
				show:true,
				backdrop:'static',
				keyboard:false
			});
		});
		$("body").trigger("PRELOAD_START", {
			count: barSelection,
			import: importText
		});
	};

	that.init = init;
	return that;
})();