var MatrixView = (function() {

	var that = {},
		matrices,
		beatCount,

	// Initialisiert das MatrixView
	init = function(count) {
		beatCount = count;
		_generateMatrices();
		_registerListeners();
	},

	// Registriert alle Listener
	_registerListeners = function(){
		$("body").on("REFRESH_ALL", _resetMatrices);
	},

	// Setzt alle Matrizen zurück (nur UI)
	_resetMatrices = function(){
		$("span").each(function(index, element){
			if(element.className === "cell clicked" || element.className === "cell clicked animation"){
				element.className = "cell unclicked animation";
				setTimeout(function(){
					element.className = "cell unclicked";
				}, 100);
			}
		});
	},

	// Generiert alle Matrizen 
	_generateMatrices = function(){
		matrices = [
			{instrument: "piano", matrices: _generateInstrumentMatrix("piano")},
			{instrument: "pizzicato", matrices: _generateInstrumentMatrix("pizzicato")},
			{instrument: "bass", matrices: _generateInstrumentMatrix("bass")},
			{instrument: "celesta", matrices: _generateInstrumentMatrix("celesta")}];

		for(var i = 0; i < matrices.length; i++){
			var matrixObj = matrices[i];
			for(var j = 0; j < matrixObj.matrices.length; j++){
				$("#beat" + j + matrixObj.instrument).append(matrixObj.matrices[j]);
			}
		}
		$("body").trigger("MATRIXVIEW_FINISHED");
	},

	// Generiert eine vollständige matrix je nach Instrument und Anzahl der Takte
	// und gibt sie in Form eines Arrays zurück
	_generateInstrumentMatrix = function(instrument){
		var result = [];

		for(var a = 0; a < (beatCount * 16); a += 16){
			var matrix = document.createElement("div");
				matrix.classList.add("matrixContainerSingle");

			var maxID = ((beatCount*256) - (16*(beatCount-1))) + a;
			for(var b = a; b < maxID; b += (16*beatCount)){
				var row = document.createElement("div");
			
				for(var c = b; c < (b + 16); c++){
					var cell = document.createElement("span");
					cell.id = c + instrument;
					cell.classList.add("cell");
					cell.classList.add("unclicked");

					row.appendChild(cell);
				}
				matrix.appendChild(row);
			}
			result[a/16] = matrix;
		}

		return result;
	};

	that.init = init;
	return that;
})();