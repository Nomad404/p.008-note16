var ExportDialog = (function(){

	var that = {},
	type = "",
	
	// Initialisiert den ExportDialog
	init = function(event) {
		type = event;
		_resizeDialog(); 
		_setDialog(); 
		$('#importExportDialog').modal({
			show:true,
			backdrop:"static",
			keyboard:true
		}); 
		_registerListeners();
	},

	// Stellt die Nachricht im Dialog ein
	_setDialog = function(){
		document.getElementById('importExportTitle').innerHTML = "Export your Project";
		document.getElementById('explanation').innerHTML = "In order to export your project, simply copy the text below and paste it into a file in order to save it. For performance reasons, we recommend you to not change the generated text afterwards."; 
	}, 

	// Passt die Position des Dialogs der Bildschirmgröße an
	_resizeDialog = function(){
		window.onload = function(){
			$dialog = $(".modal-dialog");
			var dialogHeight = parseInt($(".modal-dialog").css("height").replace("px", "")),
				screenHeight = window.screen.height;
			$(".modal-dialog").css("margin-top", ((screenHeight/2) - (dialogHeight/2) - (screenHeight*0.07)) + "px");
		}
	};

	that.init = init;
	return that;
})();
