var ControlsView = (function() {
	
	var that = {},

	// Initialisiert die ControlsView
	init = function() {
		_initUI();
		_registerListeners();
	},

	// Initialisiert die UI Elemente
	_initUI = function() {
		_initBPMHandler();
		_initVolumeHandler();
		_initRhythmHandler();
		$("body").trigger("CONTROLSVIEW_FINISHED");
	},

	//Registriert alle notwendigen Listener auf allen Elementen des ControlsView
	_registerListeners = function() {
		$("#exportButton").on("click", _exportProject);
		$("#volumeOff").on("click", _resumeSound);
		$("#volumeOn").on("click", _mute);
		$("#play").on("click", _play);
		$("#stop").on("click", _stop);
		$("#refresh").on("click", _refresh);
	},

	// Initialisiert den Handler für den BPM-Regler
	_initBPMHandler = function(){
		$("#bpmSlider").each(function () {
			var input = $(this);
			$(".output").insertAfter($(this));
		})
			.bind("slider:ready slider:changed", function (event, data) {
				$(this).nextAll(".output:first")
					.html(data.value.toFixed(0));
					$("body").trigger("BPM_CHANGE", data.value.toFixed(0));
			});
	},

	// Initialisiert den Handler für den Lautstärkeregler
	_initVolumeHandler= function(){
		$("#volumeSlider").each(function () {
			var input = $(this);
			$(".outputVol").insertAfter($(this));
		})
			.bind("slider:ready slider:changed", function (event, data) {
				$(this).nextAll(".outputVol:first")
					.html(data.value.toFixed(0));
					$("body").trigger("VOLUME_CHANGE", data.value.toFixed(0));
			});
	},

	// Initialisiert den Handler für die Rhythmusauswahl
	_initRhythmHandler = function(){
		$("#beatSamples").on("click", "label", function(){
			$("body").trigger("RHYTHM_CHANGE", $(this)[0].id);
		});
	},	

	// Startet den Export
	_exportProject = function(){
		$("body").trigger("EXPORT_START");
	},

	// Startet alle Beats und Schleifen 
	// Schaltet den Play/Stop Button um
	_play = function(){
		$("body").trigger("PLAY");
		$("#play").toggle();
		$("#stop").toggle();
	},

	// Stoppt alle Schleifen
	// Schaltet den Play/Stop Button um
	_stop = function(){
		$("body").trigger("STOP");
		$("#play").toggle();
		$("#stop").toggle();
	},

	// Startet das Zurücksetzen aller Matrizen
	// Setzt die Auswahl des Rhythmus wieder zurück (nur UI)
	_refresh = function(){
		$("body").trigger("REFRESH_ALL");
		$(".rhythmSel").removeClass("active");
		$("#rhythm0").addClass("active");
	},

	// Startet das Event für das Wiederherstellen der Lautstärke
	// Schaltet den Stummschalten-Button um
	_resumeSound = function(){
		$("body").trigger("RESUME_SOUND");
		$("#volumeOn").toggle();
		$("#volumeOff").toggle();
	},

	// Startet das Event für das globale Stummschalten
	// Schaltet den Stummschalten-Button um
	_mute = function(){
		$("body").trigger("MUTE");
		$("#volumeOn").toggle();
		$("#volumeOff").toggle(); 
	};

	that.init = init;
	return that;
})();